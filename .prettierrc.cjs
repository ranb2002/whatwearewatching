/**
 * Prettier configuration file
 * https://prettier.io/docs/en/options.html
 */
module.exports = {
  // Include parentheses around a sole arrow function parameter.
  arrowParens: "always", // Default: always

  // Put the > of a multi-line HTMLElement at the end of the last line instead of being alone on the next line
  // (does not apply to self-closing elements).
  bracketSameLine: false,

  // Print spaces between brackets in object literals
  bracketSpacing: true, // Default: true

  // Control whether Prettier formats quoted code embedded in the file.
  embeddedLanguageFormatting: "auto", // Default: "auto"

  // Ensure all end of line are "Line Feed only (\n)"
  endOfLine: "lf", // Default: "lf"

  // Specify the global whitespace sensitivity for HTML, Vue, Angular, and Handlebars.
  //    ignore: All whitespace is considered insignificant.
  htmlWhitespaceSensitivity: "ignore", // Default: "css"

  // Specify the line length that the printer will wrap on
  printWidth: 120, // Default: 80

  // If at least one property in an object requires quotes, quote all properties
  quoteProps: "consistent", // Default: "as-needed"

  // Print semicolons at the ends of statements
  semi: true, // Default: true

  // Enforce single attribute per line in HTML, Vue and JSX.
  singleAttributePerLine: true,

  // Use single quotes instead of double quotes
  singleQuote: false, // Default: false

  // Specify the number of spaces per indentation-level
  tabWidth: 2, // Default: 2, use 2 to be consistent with ESLint configuration

  // Trailing commas wherever possible (including function arguments)
  trailingComma: "all", // Default: "es5"

  // Indent lines with tabs instead of spaces
  useTabs: false, // Default: false
};
